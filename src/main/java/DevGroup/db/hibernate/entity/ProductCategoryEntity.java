package DevGroup.db.hibernate.entity;

import java.sql.Timestamp;

/**
 * Created by Светун on 13.03.2016.
 */
public class ProductCategoryEntity {
    private int id;
    private String name;
    private String opis;
    private Timestamp dateChanges;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Timestamp getDateChanges() {
        return dateChanges;
    }

    public void setDateChanges(Timestamp dateChanges) {
        this.dateChanges = dateChanges;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductCategoryEntity that = (ProductCategoryEntity) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (opis != null ? !opis.equals(that.opis) : that.opis != null) return false;
        if (dateChanges != null ? !dateChanges.equals(that.dateChanges) : that.dateChanges != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (opis != null ? opis.hashCode() : 0);
        result = 31 * result + (dateChanges != null ? dateChanges.hashCode() : 0);
        return result;
    }
}
