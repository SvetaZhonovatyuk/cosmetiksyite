package DevGroup.db.hibernate.services;


import DevGroup.db.hibernate.dao.ClientDAO;
import DevGroup.db.hibernate.impl.ClientDAOImpl;

public class Factory {
    private static Factory instance = null;

    private static ClientDAO clientDAO = null;

    public static synchronized Factory getInstance(){
        if (instance == null){
            instance = new Factory();
        }
        return instance;
    }

    public ClientDAO getClientDAO (){
        if (clientDAO == null)
        {
            clientDAO = new ClientDAOImpl();
        }
        return clientDAO;
    }


}
