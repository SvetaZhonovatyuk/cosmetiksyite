package DevGroup.db.hibernate.dao;

import DevGroup.db.hibernate.entity.ProductOrderEntity;
import DevGroup.db.hibernate.entity.ProductsEntity;

import java.util.List;

/**
 * Created by Светун on 14.03.2016.
 */
public interface ProductsDAO {
    void addProducts (ProductsEntity ProductsEntity);
    void updateProducts (ProductsEntity ProductsEntity);
    void delProducts (ProductsEntity ProductsEntity);

    ProductsEntity getProductOrderById (int idProducts);
    List<ProductsEntity> getAllProducts ();
}
