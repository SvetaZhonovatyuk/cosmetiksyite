package DevGroup.db.hibernate.dao;

import DevGroup.db.hibernate.entity.ClientEntity;

import java.util.List;

/**
 * Created by Светун on 13.03.2016.
 */
public interface ClientDAO {
    void addClient (ClientEntity clientEntity);
    void updateClient (ClientEntity clientEntity);
    void delClient (ClientEntity clientEntity);

    ClientEntity getClientById (int idClient);
    List<ClientEntity> getAllClients ();
}
