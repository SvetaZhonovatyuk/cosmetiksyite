package DevGroup.db.hibernate.dao;

import DevGroup.db.hibernate.entity.OrderEntity;
import DevGroup.db.hibernate.entity.ProductCategoryEntity;

import java.util.List;

/**
 * Created by Светун on 14.03.2016.
 */
public interface ProductCategoryDAO {
    void addProductCategory (ProductCategoryEntity ProductCategoryEntity);
    void updateProductCategory (ProductCategoryEntity ProductCategoryEntity);
    void delProductCategory (ProductCategoryEntity ProductCategoryEntity);

    ProductCategoryEntity getProductCategoryById (int idProductCategory);
    List<ProductCategoryEntity> getAllProductCategory ();
}
