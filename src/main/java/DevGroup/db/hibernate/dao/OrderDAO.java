package DevGroup.db.hibernate.dao;

import DevGroup.db.hibernate.entity.ClientEntity;
import DevGroup.db.hibernate.entity.OrderEntity;

import java.util.List;

/**
 * Created by Светун on 13.03.2016.
 */
public interface OrderDAO {
    void addOrder (OrderEntity orderEntity);
    void updateOrder (OrderEntity orderEntity);
    void delOrdert (OrderEntity orderEntity);

    OrderEntity getOrderById (int idOrder);
    List<OrderEntity> getAllOrder ();
}
