package DevGroup.db.hibernate.dao;

import DevGroup.db.hibernate.entity.ProductCategoryEntity;
import DevGroup.db.hibernate.entity.ProductOrderEntity;

import java.util.List;

/**
 * Created by Светун on 14.03.2016.
 */
public interface ProductOrderDAO {
    void addProductOrder (ProductOrderEntity ProductOrderEntity);
    void updateProductOrder (ProductOrderEntity ProductOrderEntity);
    void delProductOrder (ProductOrderEntity ProductOrderEntity);

    ProductOrderEntity getProductOrderById (int idProductOrder);
    List<ProductOrderEntity> getAllProductOrder ();
}
