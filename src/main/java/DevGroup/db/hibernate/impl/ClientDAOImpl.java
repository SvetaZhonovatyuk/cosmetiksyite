package DevGroup.db.hibernate.impl;

import DevGroup.db.hibernate.dao.ClientDAO;
import DevGroup.db.hibernate.entity.ClientEntity;
import DevGroup.db.hibernate.hutil.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Светун on 13.03.2016.
 */
public class ClientDAOImpl implements ClientDAO {
    @Override
    public void addClient(ClientEntity clientEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(clientEntity);
        session.getTransaction().commit();
    }

    @Override
    public void updateClient(ClientEntity clientEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(clientEntity);
        session.getTransaction().commit();

    }

    @Override
    public void delClient(ClientEntity clientEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(clientEntity);
        session.getTransaction().commit();

    }

    @Override
    public ClientEntity getClientById(int idClient) {
        ClientEntity clientEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        clientEntity = (ClientEntity)session.load(ClientEntity.class,idClient);
        session.getTransaction().commit();
        return clientEntity;
    }

    @Override
    public List<ClientEntity> getAllClients() {
        List<ClientEntity> clientEntityList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        clientEntityList = session.createCriteria(ClientEntity.class).list();
        session.getTransaction().commit();
        return clientEntityList;
    }
}
